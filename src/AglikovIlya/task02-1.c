#define N 40
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
    char pass[256];
    int length, i, j, count=0, x, c=0, number=0, symbol=0, TopSymbol=0;
    srand(time(0));
    puts("Enter a password's length (min length is 3)");
    scanf("%d", &length);
    if (length<3)
    {
        puts("Error - password length is too short");
        return 1;
    }
    for(j=0;j<N;j++)
        {
            do
                {
                    for(i=0;i<length;i++)
                        {
                            x=rand()%3;
                            if(x==0)
                                {
                                    number=1;
                                    pass[i]='0'+rand()%10;
                                }
                            if(x==1)
                                {
                                    symbol=1;
                                    pass[i]='A'+rand()%25;
                                }
                            if(x==2)
                                {
                                    TopSymbol=1;
                                    pass[i]='a'+rand()%25;
                                }
                        }
                    c=number+symbol+TopSymbol;
                    number=0;
                    symbol=0;
                    TopSymbol=0;
                } while(c!=3);
            if(count==2)
                {
                    count=count-2;
                    printf("\n");
                }
            if(count==1)
                printf("   ");
            for (i=0;i<length;i++)
                putchar(pass[i]);
            count++;
        }
    return 0;
}
